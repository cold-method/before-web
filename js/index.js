$(function () {
    // 定义变量
    let content = $("#content"); // 获取 id 为 "content" 的元素并存储在变量 content 中
    // 标题栏容器
    let header = $("header"); // 获取 <header> 元素并存储在变量 header 中
    // ul 容器
    let contentUL = $("#content>ul"); // 获取 id 为 "content" 下的 <ul> 元素并存储在变量 contentUL 中
    // ul 中的 li 的集合
    let contentLis = $("#content>ul>li"); // 获取 id 为 "content" 下的所有 <li> 元素并存储在变量 contentLis 中

    // 当标题栏中的菜单项被点击时执行以下函数
    $("header>.nav>li").click(function () {
        // 获取被点击菜单项的索引
        let index = $(this).index();
        // 调用 moveHead 函数，处理标题栏的相关动作
        moveHead(index);
        // 调用 moveContent 函数，处理内容区域的相关动作
        moveContent(index);
    })

    // 处理标题栏的相关动作
    function moveHead(index) {
        let current = $("header>.nav>li").eq(index);
        // 如果是第0项，切换显示的图片
        if (index == 0) {
            current.find("img").eq(0).slideUp().siblings("img").slideDown();
        } else {
            current.parent().find("img").eq(1).slideUp().siblings("img").slideDown();
        }
        // 切换显示的文本颜色和字体粗细
        current.find("a").css({ color: 'black', fontWeight: 'bold' }).parent().siblings().find("a").css({ color: "#ff8d1a", fontWeight: 'normal' });
        // 移动指示器的位置
        let ml = current.position().left + current.width() / 2 - $(".arrow").width() / 2;
        $(".arrow").animate({ left: ml }, 500);
    }

    // 处理内容区域的相关动作
    function moveContent(index) {
        // 切换显示的 content 中的 li
        let h = -index * ($(window).height() - header.height());
        contentUL.animate({ top: h }, 1000);
    }

    // 模拟点击第一项菜单
    $("header>.nav>li").eq(0).trigger('click');

    // 设置内容元素的高度
    function contentBind() {
        content.css('height', $(window).height() - header.height());
        contentLis.css('height', $(window).height() - header.height());
    }

    // 监听窗口大小改变事件
    $(window).resize(function () {
        contentBind();
    });


    var timer = null
    var showIndex = 0

    $(window).mousewheel(function (event, delta) {

        clearTimeout(timer)
        timer = setTimeout(function () {
            console.log("调用whell函数,delta=" + delta);
            // 向下
            if (delta == -1 && showIndex < contentLis.length - 1) {
                showIndex++

            }
            if (delta == 1 && showIndex > 0) {//向上
                showIndex--
            }
            moveContent(showIndex)
            moveHead(showIndex)
        }, 200)
    })

    // 轮播图
    var mySwiper = new Swiper('.swiper', {
        // direction: 'vertical', // 垂直切换选项
        loop: true, // 循环模式选项
        autoplay: true,

        autoplay: {
            disableOnInteraction: false,
            pauseOnMouseEnter: true,
        },

        // 如果需要分页器
        pagination: {
            el: '.swiper-pagination',
        },

        // 如果需要前进后退按钮
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },

        // 如果需要滚动条
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
        }
    })

    let historyText = ["朝阳寺，位于四川省达州市通川区龙爪塔风景区，因龙爪山(又名玉印山)上的龙爪塔而得名，龙爪塔属四川省重点保护文物，相传由鲁班修建", "盛不盛开，花都是花，有你没你，我都是我。", "太阳正在坠落", "风在摇曳", "山前山后各有哀愁，有风无风都不自由。", "蜘蛛侠"]
    $("#content>.list>.history>div>.pics>.item").mouseover(function () {
        //第几个item
        let index = $(this).index()
        $("#content>.list>.history>div>.text>.contentText").html(historyText[index])
    })

    // 美食部分

    $("#content>.list>.food>div>.pics>.item").click(function () {
        let index = $(this).index()
        // 找到点击的图片并进行克隆一次
        let clontImg = $(this).find("img").clone()

        clontImg.css({ width: $(this).width(), height: $(this).height(), position: 'absolute', left: $(this).position().left, top: $(this).position().top })
        $("#content>.list>.food>div>.pics").append(clontImg)

        clontImg.animate({ width: 818, height: 555, top: -100, left: 140 }, 1000)
        clontImg.click(function () {
            $(this).animate({ width: 0, height: 0 }, 1000, function () {
                $(this).remove()
            })
        })
    })

    //    风景部分
    // 定义一个存储景点名称的数组
    var senarr = ['西安城墙', "陕西历史博物馆", '大唐芙蓉园', '碑林', '大雁塔', '钟鼓楼'];

    // 遍历每个元素
    $("#content>.list>.senery>div>.pics>.item").each(function (index, item) {
        // 获取元素的"data"属性值，用于确定图片的名称
        var str = $(this).attr("data");

        // 获取每个图片容器的宽度和高度
        var pw = $(this).width();
        var ph = $(this).height();

        // 向每个item中的div.front中添加4个div，每个div中添加一个img，显示1/4图片
        for (var i = 0; i < 4; i++) {
            var div = $("<div></div>");
            div.css({ width: pw / 2, height: ph / 2, position: 'relative', overflow: 'hidden' });
            var img = $('<img></img>');

            // 计算每个图片的偏移，显示不同部分的图片
            var top = -Math.floor(i / 2) * (ph / 2);
            var left = -(i % 2) * (pw / 2);

            // 设置图片的路径和位置
            img.prop('src', `images/${str}b.jpg`).css({ top: top, left: left });

            // 将图片添加到div中
            div.append(img);

            // 将div添加到当前item的"front"容器中
            $(this).find(".front").append(div);
        }

        // 获取当前item下的所有img元素
        var imgs = $(this).find("img");

        // 鼠标悬停时，执行动画
        $(this).hover(function () {
            imgs.eq(0).css({ top: ph / 2 });
            imgs.eq(1).css({ left: -pw });
            imgs.eq(2).css({ left: pw / 2 });
            imgs.eq(3).css({ top: -ph });
        }, function () {
            imgs.eq(0).css({ top: 0 });
            imgs.eq(1).css({ left: -pw / 2 });
            imgs.eq(2).css({ left: 0 });
            imgs.eq(3).css({ top: -ph / 2 });
        });

        // 单击图片时执行的操作
        $(this).click(function () {
            console.log("点击了图片");
            console.log("create image");

            // 创建显示图片的元素
            var showPic = $("<img></img>");
            showPic.prop('src', `images/${str}b.jpg`).css({ width: 0, height: 0, position: "absolute", border: '5px solid orange', boxShadow: '5px 5px 5px gray' });

            // 将图片添加到".senery .pics"容器中
            $(".senery .pics").append(showPic);

            // 创建图片中的说明文字
            var span = $(`<span>${senarr[index]}</span>`);
            span.css({ fontSize: '30px', position: 'absolute', left: 120, top: 280, color: "orange", textShadow: "3px 3px 3px gray" });

            // 图片显示动画
            showPic.animate({ width: 800, height: 450, left: 100, top: -100 }, 1000, function () {
                $(".senery .pics").append(span).fadeIn(1000);
            });

            // 点击图片时关闭图片
            showPic.click(function () {
                $(".senery .pics span").remove();
                $(this).animate({ width: 0, height: 0 }, 1000, function () {
                    $(this).remove();
                });
            });
        });
    });


    // 第五屏js代码
        let names = ['鹖冠子', '春申君', '冯绲', '王平', '李雄', '元稹', '破山禅师', '王万邦', '唐甄']
        let imgs = $(".person .picContent img")
        let picIndex = 0
    $(".person .bodyText").html(names[picIndex])
        imgs.each((ind, item) => {
        $(item).css({ "transform": `rotateY(${ind * 40}deg) translateZ(400px)` })
    })
        $(".person .picStage").click(function () {
        picIndex++
        $(".person .bodyText").text(names[picIndex % 9])
        $(".person .picContent").css({
            "transform": `rotateY(-${picIndex * 40}deg)`
        })
    })

})
